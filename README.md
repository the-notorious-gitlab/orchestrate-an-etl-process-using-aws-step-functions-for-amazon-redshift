## Orchestrate an ETL process using AWS Step Functions for Amazon Redshift

## Introduction
Data lakes need to go through ETL (Extract, Transform, Load) to convert large amounts of information into data that we can utilize.  

This tutorial will go through the steps on how we can implement an ETL process that is loosely coupled using AWS Step Functions, AWS Lambda and AWS Batch to target an Amazon Redshift Cluster.  

## Components of the Tutorial 
**Dataset**
</br>The main part of the dataset contains information about sales from a store as well as other related information such as customer demographic and item.  
<p align="center">
    <img src="Images/Dataset.png" width="70%" height="70%">
</p>

**AWS Step Function** </br> Ensures multiple ETL jobs execute in order and completes successfully.  This means that the user does not need to make sure each job runs manually, simplifying complicated workflows.  

*Outline of the Step Function*
<p align="center">
    <img src="Images/Step_Function.png" width="50%" height=50%">
</p>

The Step Function makes sure that we transform and load columns from the dataset that we want to analyze.  From the dataset, we want to transform and load the “Customer Address”, “Item” and “Store Sales”.  The results will be stored in Redshift in the form of a table with the relevant columns.  

The order of the Step Function is as follows: 
1. ETL checks the DB connection in Step 1 
2. Once the connection is verified, the ETL process can start to transform and load the columns “Customer Address” (Step 2.1) and “Item dimension” (Step 2.2).  
3. Once Step 2 has completed, “Store Sales” (Step 3) can then go through the transform and load process.   

### CloudFormation Template 
The CloudFormation template launches the environment needed for the tutorial and contains the required AWS resources and  dataset.  

The CloudFormation template can be found in the lab titled "ETL_Step_Function.yaml"

The CloudFormation Template includes the following: 
- A VPC and associated subnets, security groups, and routes
- IAM roles
- A Amazon Redshift cluster
- An AWS Batch job definition and compute environment
- A Lambda function to submit and poll AWS Batch jobs
- A Step Functions state machine to orchestrate the ETL workflow and refresh the data in the Amazon Redshift cluster

### Architecture 
<p align="center">
    <img src="Images/Architecture.png" width="70%" height="70%">
</p>

## Tutorial 
### Create a CloudFormation Stack
- Download the CloudFormation template: [ETL_Step_Function.yaml](Files/ETL_Step_Function.yaml) 

- Open CloudFormation from the console.
  
- Click **Create Stack** 
  
- Under “Step 1: Template”, choose **"Template is Ready"** and **"Upload Template File"**.
<p align="center">
    <img src="Images/1.jpg" width="80%" height="80%">
</p>

- Click on **"Choose File"** and upload the YAML File "ETL_Step_Function.yaml"
<p align="center">
    <img src="Images/2.jpg" width="80%" height="80%">
</p>

- Click **Next**

- Under “Step 2: Specify Stack Details”, input a name for your stack 
<p align="center">
    <img src="Images/3.jpg" width="80%" height="80%">
</p>

- Create your own master user password and master username 
<p align="center">
    <img src="Images/4.jpg" width="80%" height="80%">
</p>

- Click **"Next"** until you reach the **“Review”** tab 
<p align="center">
    <img src="Images/5.png" width="80%" height="80%">
</p>

- Scroll down and tick the box: “I acknowledge that AWS CloudFormation might create IAM Resources” 

- Click **"Create Stack"** 
<p align="center">
    <img src="Images/6.png" width="80%" height="80%">
</p>

- Wait until the CloudFormation Stack status changes to “Create Complete” 
  
- Under the **"Resources"** tab in the CloudFormation Stack, take note of the **“Job Definition”** and **“Job Queue”**

<p align="center">
    <img src="Images/7.jpg" width="80%" height="80%">
</p>

<p align="center">
    <img src="Images/8.jpg" width="80%" height="80%">
</p>

- Under the **Output** tab in the CloudFormation Stack, scroll down to  **”Execution Input"** listed under “Key” and copy the JSON code listed under “Value” 

<p align="center">
    <img src="Images/9.jpg" width="80%" height="80%">
</p>
<p align="center">
    <img src="Images/10.jpg" width="80%" height="80%">
</p>

>The JSON Code has the following format 
```
{ "DBConnection":{ "jobName":"
…
alue":"s3://salamander-us-east-1/reinvent2018/ant353/etlscript/store_sales.sql" }, { "name":"DATASET_DATE", "value":"2003-01-02" } ] } } }
```

### Create AWS Batch Job 
AWS batch loads the dataset into the AWS Redshift Cluster created by the CloudFormation template.  The Redshift cluster will be used to query the data.  

- Open "AWS Batch Job" from the console 
  
- Under "Dashboard", click **"Create Job"**
<p align="center">
    <img src="Images/11.jpg" width="80%" height="80%">
</p>

- Input a Name for the AWS Batch Job 
  
- Under **"Job Definition"** and **"Job Queue"**, select the “Job Definition” and “Job Queue” from the CloudFormation stack created in the previous steps 
<p align="center">
    <img src="Images/12.jpg" width="80%" height="80%">
</p>

*For Reference*
<p align="center">
    <img src="Images/8.jpg" width="50%" height="50%">
</p>

- Leave all other settings as default 
  
- Scroll down and click **“Submit Job”**
  
- Wait for the job to complete 
  
- You can check if the Batch Job has completed by going to the "Jobs" tab and clicking on “Succeeded” 

<p align="center">
    <img src="Images/13.jpg" width="80%" height="80%">
</p>

### AWS Step Function 

The ETL process is implemented through a Step Function.  In this tutorial, the step function will focus on data from “2010-10-10”. 

- Open "Step Function" from the console 
  
- Select the Step Function created by the CloudFormation Stack. 
  > The Step Function should have a name that starts with “Job Status Poller State Machine” 
<p align="center">
    <img src="Images/14.jpg" width="80%" height="80%">
</p>

- Click **"Start Execution"**
<p align="center">
    <img src="Images/15.jpg" width="80%" height="80%">
</p>

- This will open a page **"New Execution"**
  
- Under **"Input"**, enter the JSON code from "Execution Input" in the CloudFormation Stack 
  
- Click **"Start Execution"**
<p align="center">
    <img src="Images/16.jpg" width="80%" height="80%">
</p>

*For Reference* 
<p align="center">
    <img src="Images/10.jpg" width="50%" height="50%">
</p>

- Wait for the Step Function to run 
  
- The "Execution Status" status will change to "Succeeded"
<p align="center">
    <img src="Images/17.jpg" width="80%" height="80%">
</p>

> The workflow of the Step Function is displayed below.  
>
> This workflow corresponds to the Step Function workflow at the start of the tutorial.  
<p align="center">
    <img src="Images/18.png" width="80%" height="80%">
</p>

*Step Function Workflow*
<p align="center">
    <img src="Images/Step_Function_Workflow.jpg" width="60%" height="60%">
</p>

### AWS Redshift 
AWS Redshift is used to query the data.  In the Step Function, we want to access the data from “2010-10-10”.  We can then use Redshift to display the relevant data. 

- Open Redshift from the console 
  
- Click on **“Query Editor”**
   
- Select the Redshift Cluster created with the CloudFormation Stack 
  
- For Database enter “dev” 
  
- Enter the database username and password from the CloudFormation Stack 
  
- Click **"Connect"**
<p align="center">
    <img src="Images/19.png" width="80%" height="80%">
</p>

*For Reference* 
<p align="center">
    <img src="Images/4.jpg" width="50%" height="50%">
</p>

- Under **“New Query”**, enter the following information 
> We need to enter the following information to query the data 
```
SELECT c_last_name, 
               c_first_name, 
               ca_city, 
               bought_city, 
               ss_ticket_number, 
               amt, 
               profit 
FROM   (SELECT ss_ticket_number, 
               ss_customer_sk, 
               ca_city            bought_city, 
               Sum(ss_coupon_amt) amt, 
               Sum(ss_net_profit) profit 
        FROM   store_sales, 
               date_dim, 
               store, 
               household_demographics, 
               customer_address 
        WHERE  store_sales.ss_sold_date_sk = date_dim.d_date_sk 
               AND store_sales.ss_store_sk = store.s_store_sk 
               AND store_sales.ss_hdemo_sk = household_demographics.hd_demo_sk 
               AND store_sales.ss_addr_sk = customer_address.ca_address_sk 
               AND ( household_demographics.hd_dep_count = 6 
                      OR household_demographics.hd_vehicle_count = 0 ) 
               AND d_date =  '2003-01-02'
        GROUP  BY ss_ticket_number, 
                  ss_customer_sk, 
                  ss_addr_sk, 
                  ca_city) dn, 
       customer, 
       customer_address current_addr 
WHERE  ss_customer_sk = c_customer_sk 
       AND customer.c_current_addr_sk = current_addr.ca_address_sk 
       AND current_addr.ca_city <> bought_city 
ORDER  BY c_last_name, 
          c_first_name, 
          ca_city, 
          bought_city, 
          ss_ticket_number
LIMIT 100;
```

<p align="center">
    <img src="Images/20.png" width="80%" height="80%">
</p>

- Click **“Run Query”**
  
- This query should display all data for “2010-10-10” 
<p align="center">
    <img src="Images/21.png" width="80%" height="80%">
</p>

## Conclusion
This tutorial goes through the steps of implementing an ETL process through a Step Function workflow and querying the data using AWS services.  

This tutorial can also be expanded further to fit the requirements of the user.  The ETL Process of this tutorial was invoked manually using a step function.  A step function can also be invoked automatically through S3 Events, such as when there is new data uploaded into an S3 bucket, CloudWatch events or by setting a schedule.    

## Delete AWS Resources 
- Delete AWS CloudFormation Stack and AWS Batch Job 